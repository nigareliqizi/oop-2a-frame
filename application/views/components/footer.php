	<footer class="site-footer clearfix">
	<a href="#top" id="back-to-top" class="back-top"></a>
	<div class="text-center">
		<a href="index.html">Nectaria Template</a> &copy; 2016<br/>
		Free HTML Template by <a href="https://wowthemes.net">WowThemes.net</a>
	</div>
	</footer>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?= base_url()?>/assets/js/masonry.min.js"></script>
<script type="text/javascript" src="<?= base_url()?>/assets/js/jquery.fitvids.js"></script>
<script type="text/javascript" src="<?= base_url()?>/assets/js/index.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/fontawesome.min.js"></script>
</body>
</html>